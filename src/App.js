import React, { Component } from 'react';
import './App.css';
import './helpers/API';
import CreateSurvivor from './components/CreateSurvivor';
import UpdateSurvivor from './components/UpdateSurvivor';
import ReportSurvivor from './components/ReportSurvivor';
import TradeItems from './components/TradeItems';
import StatisticsReports from './components/StatisticsReports';
import { Route, Switch, Link } from 'react-router-dom';

class App extends Component {
    constructor() {
        super();
        this.state = { currentUser: '' };
    }

    updateCurrentUser = userId => {
        this.setState({ currentUser: userId });
    };

    render() {
        return (
            <div className="App">
                <div className="wrapper">
                    <header className="header">
                        <h1 className="app-title">ZSSN_</h1>
                        <nav className="menu">
                            <Link to="/">Add Survivor</Link>
                            <Link to="/update">Update Location</Link>
                            <Link to="/flag">Flag Infected</Link>
                            <Link to="/trade">Trade Items</Link>
                            <Link to="/reports">Reports</Link>
                        </nav>
                    </header>
                    <main className="main-wrapper">
                        <Switch>
                            <Route exact path="/" component={CreateSurvivor} />
                            <Route path="/update" component={UpdateSurvivor} />
                            <Route path="/flag" component={ReportSurvivor} />
                            <Route path="/trade" component={TradeItems} />
                            <Route
                                path="/reports"
                                component={StatisticsReports}
                            />
                        </Switch>
                    </main>
                </div>
            </div>
        );
    }
}

export default App;
