/* This is a helper for API calls */

const API = 'http://zssn-backend-example.herokuapp.com/api/';

function _handleErrors(response) {
    if (!response.ok) throw Error(response.statusText);
    else {
        return response;
    }
}

function _get(endpoint) {
    return fetch(API + endpoint, { method: 'GET' })
        .then(_handleErrors)
        .then(response => response.json())
        .catch(console.error);
}

function _post(endpoint, payload) {
    return fetch(API + endpoint, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
        .then(_handleErrors)
        .then(response => response)
        .catch(console.error);
}

function _patch(endpoint, payload) {
    return fetch(API + endpoint, {
        method: 'PATCH',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
    })
        .then(_handleErrors)
        .then(response => response)
        .catch(console.error);
}

/* List all people, infected or healthy */
export function getSurvivors() {
    return _get('people.json');
}

/* Inserts a new survivor */
export function postSurvivor(newSurvivor) {
    return _post('people.json', newSurvivor);
}

/* Update a survivor's location */
export function updateSurvivorLocation(uniqueId, newLocation) {
    return _get('people/' + uniqueId + '.json').then(survivorData => {
        if (!survivorData['infected?']) {
            survivorData.lonlat = 'Point(' + newLocation + ')';
            return _patch('people/' + uniqueId + '.json', survivorData);
        } else {
            return {
                result: 'failure',
                message: 'The person is infected.'
            };
        }
    });
}

/* Get all survivors that match the given keyword */
export function searchSurvivor(keyword) {
    let resultSet = _get('people.json').then(data => {
        /* Regular expression to match multiple keywords */
        let preparedQuery = new RegExp(
            '(?:' + keyword.split(' ').join('|') + ')',
            'gi'
        );
        return data.filter(v => v.name.match(preparedQuery));
    });
    return resultSet;
}

/* Flag a survivor as infected */
export function reportSurvivor(userId, infectedId) {
    return _post(
        'people/' + userId + '/report_infection.json',
        infectedId
    ).then(data => {
        if (data) {
            return {
                result: 'success',
                message: 'Survivor has been successfully reported as infected.'
            };
        } else {
            return {
                result: 'failure',
                message: 'You already have reported this survivor as infected.'
            };
        }
    });
}

/* Trade items between survivors */
export function tradeItems(personId, tradeRequest) {
    return _post(
        'people/' + personId + '/properties/trade_item.json',
        tradeRequest
    ).then(data => {
        if (data) {
            return {
                result: 'success',
                message: 'The trade has been completed!'
            };
        } else {
            return {
                result: 'failure',
                message:
                    'There was a problem with the trade it has been not completed.'
            };
        }
    });
}

/* Get average of people's items */
export function getAveragePeopleItems() {
    return _get('report/people_inventory.json').then(response => response);
}

/* Get total of points lost because of infected survivors */
export function getTotalPointsLost() {
    return _get('report/infected_points.json').then(response => response);
}
