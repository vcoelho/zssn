import React, { Component } from 'react';
import { postSurvivor } from '../helpers/API';
import Inventory from './Inventory';
import GoogleMaps from './GoogleMaps';
class CreateSurvivor extends Component {
    constructor() {
        super();

        this.state = {
            newInventory: { water: 1, food: 1, medication: 1, ammunition: 1 },
            location: { lat: '', lng: '' },
            message: ''
        };

        this.name = React.createRef();
        this.age = React.createRef();
        this.gender = React.createRef();
        this.long_lat = React.createRef();
    }

    updateNewInventory = inventory => {
        this.setState({ newInventory: inventory });
    };

    handleCoordinates = coord => {
        return this.setState({
            location: {
                lat: coord.lat,
                lng: coord.lng
            }
        });
    };

    createSurvivor = event => {
        event.preventDefault();
        let newInventory =
            'Water:' +
            this.state.newInventory.water +
            ';Food:' +
            this.state.newInventory.food +
            ';Medication:' +
            this.state.newInventory.medication +
            ';Ammunition:' +
            this.state.newInventory.ammunition;

        let newSurvivor = {
            person: {
                name: this.name.current.value,
                age: this.age.current.value,
                gender: this.gender.current.value,
                lonlat: this.long_lat.current.value
            },
            items: newInventory
        };
        return postSurvivor(newSurvivor)
            .then(response => {
                if (response) {
                    return response.json();
                } else {
                    return this.setState({
                        message: 'This name is already taken.'
                    });
                }
            })
            .then(data =>
                this.setState({
                    message:
                        'The person has been successfully registered with the unique identifier: ' +
                        data.id
                })
            );
    };

    render() {
        return (
            <React.Fragment>
                <h2>Register a new Survivor</h2>
                <h3>{this.state.message}</h3>
                <form
                    className="form-create-survivor"
                    onSubmit={this.createSurvivor}
                >
                    <div className="two-column">
                        <div className="half">
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    ref={this.name}
                                    defaultValue={
                                        'Gang Starr the ' + Math.random()
                                    }
                                />
                            </div>
                            <div className="two-column">
                                <div className="form-group">
                                    <label htmlFor="age">Age</label>
                                    <input
                                        type="text"
                                        name="age"
                                        ref={this.age}
                                        defaultValue="30"
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="gender">Gender</label>
                                    <select name="gender" ref={this.gender}>
                                        <option
                                            defaultChecked
                                            name="gender"
                                            value="M"
                                        >
                                            Male
                                        </option>
                                        <option name="gender" value="F">
                                            Female
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="half">
                            <Inventory
                                inventoryHandler={this.updateNewInventory}
                                currentInventory={this.state.newInventory}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="long_lat">
                            Click on the map to set your location
                        </label>
                        <input
                            type="text"
                            name="long_lat"
                            ref={this.long_lat}
                            readOnly
                            value={
                                'Point (' +
                                this.state.location.lat +
                                ' ' +
                                this.state.location.lng +
                                ')'
                            }
                        />
                    </div>
                    <GoogleMaps handleCoordinates={this.handleCoordinates} />
                    <button className="form-submit">Add Survivor</button>
                </form>
            </React.Fragment>
        );
    }
}

export default CreateSurvivor;
