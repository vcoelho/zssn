import React, { Component } from 'react';
import { updateSurvivorLocation } from '../helpers/API';
import GoogleMaps from '../components/GoogleMaps';

class UpdateSurvivor extends Component {
    constructor() {
        super();
        this.uniqueId = React.createRef();
        this.newLocation = React.createRef();
        this.state = { location: { lat: '', lng: '' } };
    }

    updateSurvivorLocation = event => {
        event.preventDefault();
        let uniqueId = this.uniqueId.current.value;
        let newLocation = this.newLocation.current.value;
        return updateSurvivorLocation(uniqueId, newLocation);
    };

    handleCoordinates = coord => {
        return this.setState({
            location: {
                lat: coord.lat,
                lng: coord.lng
            }
        });
    };

    render() {
        /* TODO: get the coordinates from the fucking map */
        return (
            <React.Fragment>
                <h2>Update your location</h2>
                <form
                    className="form-update-location"
                    onSubmit={this.updateSurvivorLocation}
                >
                    <label htmlFor="new-location">Your unique id</label>
                    <input
                        type="text"
                        name="unique-id"
                        defaultValue="db4bcdb2-6e75-462c-b33e-6da8aeffc6bf"
                        ref={this.uniqueId}
                    />
                    <label htmlFor="new-location">New location</label>
                    <input
                        type="text"
                        name="new-location"
                        ref={this.newLocation}
                        readOnly
                        value={
                            'Point (' +
                            this.state.location.lat +
                            ' ' +
                            this.state.location.lng +
                            ')'
                        }
                    />
                    <GoogleMaps handleCoordinates={this.handleCoordinates} />
                    <button>Update location</button>
                </form>
            </React.Fragment>
        );
    }
}

export default UpdateSurvivor;
