import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

class GoogleMaps extends Component {
    constructor() {
        super();
        this.state = {
            lat: '',
            lng: ''
        };
    }
    static defaultProps = {
        center: { lat: 40.744679, lng: -73.948542 },
        zoom: 11
    };

    handleClick = event => {
        return this.props.handleCoordinates(event);
    };

    render() {
        return (
            <div
                className="google-map"
                style={{ height: '200px', width: '100%' }}
            >
                <GoogleMapReact
                    onClick={e => this.handleClick(e)}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    markers={this.state.markers}
                />
            </div>
        );
    }
}

export default GoogleMaps;
