import React, { Component } from 'react';
import { reportSurvivor } from '../helpers/API';

class ReportSurvivorResult extends Component {
    constructor() {
        super();
        this.infected = React.createRef();
        this.state = {
            canReport: true,
            resultMessage: {
                result: '',
                message: ''
            }
        };
    }

    checkInfected = () => {
        this.setState({ canReport: !this.infected.current.checked });
    };

    reportAsInfected = infected_uId => {
        let infected = {
            infected: infected_uId
        };
        return reportSurvivor(
            '1ebe584c-98bd-43fb-b606-a1f50ea31344',
            infected
        ).then(result => {
            this.setState({ resultMessage: result });
        });
    };

    render() {
        const { uId, name } = this.props;
        return (
            <li>
                <p>{name}</p>
                <label htmlFor={'infected-' + uId}>
                    <input
                        type="checkbox"
                        id={'infected-' + uId}
                        ref={this.infected}
                        onClick={this.checkInfected}
                    />
                    Are you sure?
                </label>
                <button
                    disabled={this.state.canReport}
                    onClick={() => this.reportAsInfected(uId)}
                >
                    Report
                </button>
                <p
                    className={
                        'result-message ' + this.state.resultMessage.result
                    }
                >
                    {this.state.resultMessage.message}
                </p>
            </li>
        );
    }
}

export default ReportSurvivorResult;
