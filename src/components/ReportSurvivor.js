import React, { Component } from 'react';
import { searchSurvivor } from '../helpers/API';
import ReportSurvivorResult from './ReportSurvivorResult';

class ReportSurvivor extends Component {
    constructor() {
        super();
        this.name = React.createRef();
        this.state = {
            survivorSearchResults: {}
        };
    }
    reportSurvivor = event => {
        event.preventDefault();

        let name = this.name.current.value;
        searchSurvivor(name).then(result =>
            this.setState({ survivorSearchResults: result })
        );
    };
    render() {
        return (
            <React.Fragment>
                <h2>Report an infected person</h2>
                <form onSubmit={this.reportSurvivor}>
                    <label htmlFor="name">Survivor's name</label>
                    <input type="text" name="name" required ref={this.name} />
                    <button>Search</button>
                </form>
                <ul className="search-results">
                    {Object.keys(this.state.survivorSearchResults).map(k => {
                        let s = this.state.survivorSearchResults[k];
                        let uId = s.location.split(
                            'http://zssn-backend-example.herokuapp.com/api/people/'
                        );
                        return (
                            <ReportSurvivorResult
                                key={uId[1]}
                                uId={uId[1]}
                                name={s.name}
                            />
                        );
                    })}
                </ul>
            </React.Fragment>
        );
    }
}

export default ReportSurvivor;
