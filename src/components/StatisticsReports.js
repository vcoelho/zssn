import React, { Component } from 'react';
import {
    getSurvivors,
    getAveragePeopleItems,
    getTotalPointsLost
} from '../helpers/API';

class StatisticsReports extends Component {
    constructor() {
        super();
        this.state = {
            survivorCount: '',
            infectedPercentage: '',
            healthyPercentage: '',
            itemAverage: '',
            lostPoints: ''
        };
    }

    componentDidMount() {
        getSurvivors().then(data => {
            let infected = data.filter(v => v['infected?'] === true).length;

            this.setState({
                /* Since we need the total of people to make sense of the averages,extracting with a single API call the infected count and total of people and then figuring out the rest saves us from making 2 extra requests.   */
                infectedPercentage:
                    parseFloat((infected * 100) / data.length).toFixed(2) + '%',
                survivorCount: data.length,
                healthyPercentage:
                    parseFloat(
                        ((data.length - infected) * 100) / data.length
                    ).toFixed(2) + '%'
            });

            /* Extracting the total of each type of items the survivors would require hundreds of requests. It would be reckeless to go this way.  */
            getAveragePeopleItems().then(data => {
                this.setState({
                    itemAverage:
                        (data.report.average_items_quantity_per_person *
                            this.state.survivorCount) /
                        4
                });
            });
        });

        getTotalPointsLost().then(data => {
            this.setState({
                lostPoints: data.report.total_points_lost
            });
        });
    }

    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <blockquote>
                        <p className="statistic">
                            {this.state.infectedPercentage}
                        </p>{' '}
                        of the survivors are infected
                    </blockquote>
                    <blockquote>
                        <p className="statistic">
                            {this.state.healthyPercentage}
                        </p>{' '}
                        of the survivors are healthy
                    </blockquote>
                </div>
                <div className="container">
                    <blockquote>
                        <p className="statistic">{this.state.itemAverage}</p> of
                        water, food, medication and ammunition per survivor
                    </blockquote>
                    <blockquote>
                        <p className="statistic">{this.state.lostPoints}</p>{' '}
                        points lost due to infected people
                    </blockquote>
                </div>
            </React.Fragment>
        );
    }
}

export default StatisticsReports;
