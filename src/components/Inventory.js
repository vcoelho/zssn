import React, { Component } from 'react';

class Inventory extends Component {
    inventoryHandler = event => {
        let type = event.target.id;
        let value = parseInt(event.target.value, 10);
        let updatedInventory = this.props.currentInventory;
        updatedInventory[type] = value;
        return this.props.inventoryHandler(updatedInventory);
    };

    render() {
        return (
            <fieldset>
                <legend>Inventory</legend>
                <div className="two-column">
                    <div className="form-group">
                        <label htmlFor="water">Water</label>
                        <input
                            type="number"
                            id="water"
                            min="0"
                            onChange={this.inventoryHandler}
                            defaultValue={this.props.currentInventory.water}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="water">Food</label>
                        <input
                            type="number"
                            id="food"
                            min="0"
                            onChange={this.inventoryHandler}
                            defaultValue={this.props.currentInventory.food}
                        />
                    </div>
                </div>
                <div className="two-column">
                    <div className="form-group">
                        <label htmlFor="water">Medication</label>
                        <input
                            type="number"
                            id="medication"
                            min="0"
                            onChange={this.inventoryHandler}
                            defaultValue={
                                this.props.currentInventory.medication
                            }
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="water">Ammunition</label>
                        <input
                            type="number"
                            id="ammunition"
                            min="0"
                            onChange={this.inventoryHandler}
                            defaultValue={
                                this.props.currentInventory.ammunition
                            }
                        />
                    </div>
                </div>
            </fieldset>
        );
    }
}

export default Inventory;
