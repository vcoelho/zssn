import React, { Component } from 'react';
import Inventory from './Inventory';
import { tradeItems } from '../helpers/API';

class TradeItems extends Component {
    constructor() {
        super();
        this.state = {
            pick: { water: 1, food: 1, medication: 1, ammunition: 1 },
            payment: { water: 1, food: 1, medication: 1, ammunition: 1 },
            pickScore: '',
            paymentScore: '',
            confirmTrade: false,
            allowTrade: false,
            resultMessage: {
                result: '',
                message: ''
            }
        };

        this.userId = React.createRef();
        this.recieverName = React.createRef();
    }

    updatePick = newValue => {
        this.setState({ pick: { ...newValue } });
    };

    updatePayment = newValue => {
        this.setState({ payment: { ...newValue } });
    };

    checkTrade = event => {
        this.setState({
            confirmTrade: event.target.checked
        });
    };

    submitTrade = () => {
        let pick =
            'Water:' +
            this.state.pick.water +
            ';Food:' +
            this.state.pick.food +
            ';Medication:' +
            this.state.pick.medication +
            ';Ammunition:' +
            this.state.pick.ammunition;

        let payment =
            'Water:' +
            this.state.payment.water +
            ';Food:' +
            this.state.payment.food +
            ';Medication:' +
            this.state.payment.medication +
            ';Ammunition:' +
            this.state.payment.ammunition;

        let tradeRequest = {
            consumer: {
                name: this.recieverName.current.value,
                pick: pick,
                payment: payment
            }
        };
        return tradeItems(this.userId.current.value, tradeRequest).then(data =>
            this.setState({ resultMessage: data })
        );
    };

    static getDerivedStateFromProps(prevState, nextProps) {
        let scores = {
            pickScore:
                nextProps.pick.water * 4 +
                nextProps.pick.food * 3 +
                nextProps.pick.medication * 2 +
                nextProps.pick.ammunition * 1,
            paymentScore:
                nextProps.payment.water * 4 +
                nextProps.payment.food * 3 +
                nextProps.payment.medication * 2 +
                nextProps.payment.ammunition * 1
        };
        let allowTrade = scores.paymentScore === scores.pickScore;
        return { ...scores, allowTrade };
    }

    render() {
        return (
            <React.Fragment>
                <h2>Trade items between</h2>
                <div className="container">
                    <div className="form-group half">
                        <label htmlFor="">Your user id</label>
                        <input
                            type="text"
                            ref={this.userId}
                            defaultValue="5c56d132-d099-4016-8f9a-1d50ed69f3af"
                        />
                        <br />
                        <Inventory
                            inventoryHandler={this.updatePick}
                            currentInventory={this.state.pick}
                        />
                        <p>Total of points: {this.state.pickScore}</p>
                    </div>
                    <p>{this.pickScore}</p>
                    <div className="form-group half">
                        <label htmlFor="">Person to Recieve</label>
                        <input
                            type="text"
                            ref={this.recieverName}
                            defaultValue="Reciever"
                        />
                        <Inventory
                            inventoryHandler={this.updatePayment}
                            currentInventory={this.state.payment}
                        />
                        <p>Total of points: {this.state.paymentScore}</p>
                    </div>
                </div>
                <label htmlFor="confirm-trade">
                    <input
                        type="checkbox"
                        id="confirm-trade"
                        onClick={this.checkTrade}
                    />
                    Confirm trade
                </label>
                <button
                    ref="submit-trade"
                    disabled={
                        (this.state.allowTrade && this.state.confirmTrade) ===
                        true
                            ? false
                            : true
                    }
                    onClick={this.submitTrade}
                >
                    Trade items
                </button>
                <p>{this.state.resultMessage.message}</p>
            </React.Fragment>
        );
    }
}

export default TradeItems;
